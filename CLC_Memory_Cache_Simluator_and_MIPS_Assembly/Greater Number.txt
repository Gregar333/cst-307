############################################################
# Program: Greater Number		
# By: Cameron Brown			
# Date: 20 September 2018		
############################################################
# Description: This program will take two numbers from user
# input and determine which is greater. It will then print 
# the larger number.
############################################################

.data
	
intro: .asciiz "This program will tell you which number is greater."	
message1: .asciiz "\nWhat is the first number? "
message2: .asciiz "What is the second number? "
greaterMessage: .asciiz "The greater number is: "

.text
main:
	# Prints the intro message
	la $a0, intro
	li $v0,4
	syscall

	# Asks the user for the first number
	la $a0, message1
	li $v0,4
	syscall

	# Collects user input and assigns to a register
	li $v0,5
	syscall
	move $t0,$v0

	# Asks the user for the second number	
	la $a0, message2
	li $v0,4
	syscall
	
	# Collects user input and assigns to a register	
	li $v0,5
	syscall
	move $t1,$v0
	
	# Prints greaterMessage
	li $v0,4
	la $a0, greaterMessage
	syscall
	
	# Compares the numbers, if the first is greater it jumps to firstBig	
	bge $t0, $t1, firstBig

	# Prints the second number, if the first was larger this is skipped.
	li $v0, 1
	move $t2, $t1
	move $a0, $t1
	syscall
	
	# Closes the program
	li $v0, 10
	syscall
	
firstBig:
	# Prints the first number
	li $v0, 1
	move $t2, $t0
	move $a0, $t0
	syscall
	
	# Closes the program
	li $v0, 10
	syscall
